package com.alan.gym.ServiceTest;

import com.alan.gym.entity.Trainer;
import com.alan.gym.repository.TrainerRepository;
import com.alan.gym.service.TrainerService;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TrainerServiceTest {

    @Mock
    private TrainerRepository trainerRepository;

    @InjectMocks
    private TrainerService trainerService;

    @Test
    public void testGenerateUsername() {
        String first_name = "Alan";
        String last_name = "Gonzalez";

        Mockito.when(trainerRepository.existsByUser_userName(Mockito.anyString())).thenReturn(true);

        // Ejecutar el método que se está probando
        String generatedUsername = trainerService.generateUsername(first_name, last_name);

        // Verificar que se haya generado un nombre de usuario único
        assertNotEquals("Alan.Gonzalez", generatedUsername);
        assertTrue(generatedUsername.startsWith("Alan.Gonzalez"));
    }

    @Test
    public void testGeneratePassword() {
        String generatedPassword = trainerService.generatePassword();
        assertEquals(10, generatedPassword.length());
    }

    @Test
    public void testReadTrainer() {
        // Configuración de datos de prueba
        Trainer trainer1 = new Trainer();
        Trainer trainer2 = new Trainer();
        List<Trainer> trainers = new ArrayList<>();
        trainers.add(trainer1);
        trainers.add(trainer2);
        when(trainerRepository.findAll()).thenReturn(trainers);
        List<Trainer> result = trainerService.readTrainer();
        assertEquals(trainers, result);
    }
}
