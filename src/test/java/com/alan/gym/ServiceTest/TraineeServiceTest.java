package com.alan.gym.ServiceTest;

import com.alan.gym.entity.Trainee;
import com.alan.gym.repository.TraineeRepository;
import com.alan.gym.service.TraineeService;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TraineeServiceTest {
    @Mock
    private TraineeRepository traineeRepository;

    @InjectMocks
    private TraineeService traineeService;

    @Test
    public void testGenerateUsername() {
        String firstName = "Alan";
        String lastName = "Gonzalez";

        Mockito.when(traineeRepository.existsByUserName(Mockito.anyString())).thenReturn(true);

        String generatedUsername = traineeService.generateUsername(firstName, lastName);

        assertNotEquals("Alan.Gonzalez", generatedUsername);
        assertTrue(generatedUsername.startsWith("Alan.Gonzalez"));
    }

    @Test
    public void testGeneratePassword() {
        String generatedPassword = traineeService.generatePassword();
        assertEquals(10, generatedPassword.length());
    }
    
    @Test
    public void testSaveTrainee() {
        // Configuración de datos de prueba
        Trainee trainee = new Trainee();
        trainee.setAddress("guadalajara");
        when(traineeRepository.save(trainee)).thenReturn(trainee);
        traineeService.saveTrainee(trainee);
        verify(traineeRepository, times(1)).save(trainee);
    }

    @Test
    public void testReadTrainee() {
        // Configuración de datos de prueba
        Trainee trainee1 = new Trainee();
        Trainee trainee2 = new Trainee();
        List<Trainee> trainees = new ArrayList<>();
        trainees.add(trainee1);
        trainees.add(trainee2);
        when(traineeRepository.findAll()).thenReturn(trainees);
        List<Trainee> result = traineeService.readTrainee();
        assertEquals(trainees, result);
    }

}
