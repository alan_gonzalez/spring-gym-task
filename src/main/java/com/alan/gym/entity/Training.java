package com.alan.gym.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "training")
public class Training {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "training_id", unique = true, nullable = false)
    private int training_id;
    private String training_name;
    public Date training_date;
    public double training_duration;

    @ManyToOne
    @JoinColumn(name = "Trainee_trainee_id", referencedColumnName = "trainee_id")
    public Trainee trainee;

    @ManyToOne
    @JoinColumn(name = "Traineer_trainer_id", referencedColumnName = "trainer_id")
    public Trainer trainer;

    @ManyToOne
    @JoinColumn(name = "TrainingType_training_type_id", referencedColumnName = "training_type_id")
    public TrainingType trainingType;

    public Training() {
        
    }

    public Training(int training_id, String training_name, Date training_date, double training_duration, Trainee trainee, Trainer trainer, TrainingType trainingType) {
        this.training_id = training_id;
        this.training_name = training_name;
        this.training_date = training_date;
        this.training_duration = training_duration;
        this.trainee = trainee;
        this.trainer = trainer;
        this.trainingType = trainingType;
    }

    public int getTraining_id() {
        return training_id;
    }

    public String getTraining_name() {
        return training_name;
    }

    public void setTraining_name(String training_name) {
        this.training_name = training_name;
    }

    public Date getTraining_date() {
        return training_date;
    }

    public void setTraining_date(Date training_date) {
        this.training_date = training_date;
    }

    public double getTraining_duration() {
        return training_duration;
    }

    public void setTraining_duration(double training_duration) {
        this.training_duration = training_duration;
    }

    public Trainee getTrainee() {
        return trainee;
    }

    public void setTrainee(Trainee trainee) {
        this.trainee = trainee;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public TrainingType getTrainingType() {
        return trainingType;
    }

    public void setTrainingType(TrainingType trainingType) {
        this.trainingType = trainingType;
    }

}
