package com.alan.gym.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "trainee")
public class Trainee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trainee_id", unique = true, nullable = false)
    private int trainee_id;
    private String address;
    private Date date_birth;

    @ManyToOne
    @JoinColumn(name = "User_idUser", referencedColumnName = "user_id")
    private User user;

    public Trainee() {

    }

    public Trainee(int trainee_id, String address, Date date_birth, User user) {
        this.trainee_id = trainee_id;
        this.address = address;
        this.date_birth = date_birth;
        this.user = user;
    }

    public int getTrainee_id() {
        return trainee_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDate_birth() {
        return date_birth;
    }

    public void setDate_birth(Date date_birth) {
        this.date_birth = date_birth;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Trainee [trainee_id=" + trainee_id + ", address=" + address + ", date_birth=" + date_birth + ", user=" + user
                + "]";
    }

}
