package com.alan.gym.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "trainer")
public class Trainer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trainer_id", unique = true, nullable = false)
    private int trainer_id;

    @ManyToOne
    @JoinColumn(name = "User_idUser", referencedColumnName = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "TrainingType_training_type_id", referencedColumnName = "training_type_id")
    public TrainingType training_type;

    public Trainer() {

    }

    public Trainer(int trainer_id, User user, TrainingType trainingType) {
        this.trainer_id = trainer_id;
        this.user = user;
        this.training_type = trainingType;
    }

    public int getTrainer_id() {
        return trainer_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public TrainingType getTrainingType() {
        return training_type;
    }

    public void setTrainingType(TrainingType trainingType) {
        this.training_type = trainingType;
    }

    @Override
    public String toString() {
        return "Trainer [trainer_id=" + trainer_id + ", user=" + user + ", trainingType=" + training_type + "]";
    }

}
