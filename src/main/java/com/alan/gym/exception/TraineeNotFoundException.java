package com.alan.gym.exception;

public class TraineeNotFoundException extends Exception {

    public TraineeNotFoundException(String message) {
        super(message);
    }
}
