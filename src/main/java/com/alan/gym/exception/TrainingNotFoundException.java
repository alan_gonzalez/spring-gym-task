package com.alan.gym.exception;

public class TrainingNotFoundException extends Exception {

    public TrainingNotFoundException(String message) {
        super(message);
    }
}
