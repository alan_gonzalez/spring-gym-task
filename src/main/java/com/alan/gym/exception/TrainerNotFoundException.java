package com.alan.gym.exception;

public class TrainerNotFoundException extends Exception {

    public TrainerNotFoundException(String message) {
        super(message);
    }
}
