package com.alan.gym.service;

import com.alan.gym.entity.Trainer;
import com.alan.gym.entity.TrainingType;
import com.alan.gym.entity.User;
import com.alan.gym.exception.TrainerNotFoundException;
import com.alan.gym.repository.TrainerRepository;
import java.util.List;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainerService {

    private TrainerRepository trainerRepository;

    @Autowired
    public TrainerService(TrainerRepository trainerRepository) {
        this.trainerRepository = trainerRepository;
    }

    public void saveTrainer(Trainer trainer) {
        trainerRepository.save(trainer);
    }

    public List<Trainer> readTrainer() {
        return trainerRepository.findAll();
    }

    public Trainer deleteTrainerById(Long trainer_id) throws TrainerNotFoundException {
        Trainer temp = null;

        if (trainerRepository.existsById(trainer_id)) {
            temp = trainerRepository.findById(trainer_id).get();
            trainerRepository.deleteById(trainer_id);
        } else {
            throw new TrainerNotFoundException("Trainer with id " + trainer_id + "not found");
        }
        return temp;
    }

    public Trainer updateTrainer(Long trainer_id, User user, TrainingType training_type) throws TrainerNotFoundException {
        Trainer trainer = trainerRepository.findById(trainer_id)
                .orElseThrow(() -> new TrainerNotFoundException("Trainer with id " + trainer_id + "not found"));

        trainer.setUser(user);
        trainer.setTrainingType(training_type);

        return trainerRepository.save(trainer);
    }

    public String generateUsername(String first_name, String last_name) {
        String baseUsername = first_name + "." + last_name;
        int serialNumber = 1;
        while (usernameExists(baseUsername + serialNumber)) {
            serialNumber++;
        }
        return baseUsername + serialNumber;
    }

    private boolean usernameExists(String username) {
        return trainerRepository.existsByUser_userName(username);
    }

    public String generatePassword() {

        int length = 10;
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuilder password = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            password.append(characters.charAt(random.nextInt(characters.length())));
        }

        return password.toString();
    }

}
