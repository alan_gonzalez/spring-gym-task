package com.alan.gym.service;

import com.alan.gym.entity.Trainee;
import com.alan.gym.entity.Trainer;
import com.alan.gym.entity.Training;
import com.alan.gym.entity.TrainingType;
import com.alan.gym.exception.TrainingNotFoundException;
import com.alan.gym.repository.TrainingRepository;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingService {

    private static final Logger logger = Logger.getLogger(TrainingService.class.getName());

    private TrainingRepository trainingRepository;

    @Autowired
    public TrainingService(TrainingRepository trainingRepository) {

        this.trainingRepository = trainingRepository;
    }

    public List<Training> readTraining() {
        return trainingRepository.findAll();
    }

    public Training updateTraining(Long training_id, String training_name, Date training_date, Trainee trainee, Trainer trainer, TrainingType training_type) throws TrainingNotFoundException {
        Training training = trainingRepository.findById(training_id)
                .orElseThrow(() -> new TrainingNotFoundException("Training not found with ID: " + training_id));

        training.setTraining_name(training_name);
        training.setTraining_date(training_date);
        training.setTrainer(trainer);
        training.setTrainee(trainee);
        training.setTrainingType(training_type);

        logger.info("Logger info from deleteTraining method");

        return trainingRepository.save(training);

    }

}
