package com.alan.gym.service;

import com.alan.gym.entity.User;
import com.alan.gym.exception.UserNotFoundException;
import com.alan.gym.repository.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void saveUser(User user) {
        userRepository.save(user);
    }

    public List<User> readUser() {
        return userRepository.findAll();
    }

    public User deleteUserById(Long user_id) throws UserNotFoundException {

        User userTemp = null;

        if (userRepository.existsById(user_id)) {
            userTemp = userRepository.findById(user_id).get();
            userRepository.deleteById(user_id);
        } else {
            throw new UserNotFoundException("User with id " + user_id + "not found");
        }
        return userTemp;
    }

    public User updateUser(Long user_id, String first_name, String last_name, String username, String password, boolean is_active) throws UserNotFoundException {
        User user = userRepository.findById(user_id)
                .orElseThrow(() -> new UserNotFoundException("User with id " + user_id + "not found"));

        user.setFirst_name(first_name);
        user.setLast_name(last_name);
        user.setUsername(username);
        user.setPassword(password);
        user.setIs_active(is_active);

        return userRepository.save(user);
    }

}
