package com.alan.gym.service;

import com.alan.gym.entity.Trainee;
import com.alan.gym.entity.User;
import com.alan.gym.exception.TraineeNotFoundException;
import com.alan.gym.repository.TraineeRepository;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.logging.Logger;

@Service
public class TraineeService {

    private static final Logger logger = Logger.getLogger(TraineeService.class.getName());

    private TraineeRepository traineeRepository;

    @Autowired
    public TraineeService(TraineeRepository traineeRepository) {
        this.traineeRepository = traineeRepository;
    }

    public void saveTrainee(Trainee trainee) {
        traineeRepository.save(trainee);
        logger.info("Logger info from saveTrainee method");
    }

    public List<Trainee> readTrainee() {
        return traineeRepository.findAll();
    }

    public Trainee deleteTraineeById(Long trainee_id) throws TraineeNotFoundException {

        Trainee temp = null;

        if (traineeRepository.existsById(trainee_id)) {
            temp = traineeRepository.findById(trainee_id).get();
            traineeRepository.deleteById(trainee_id);;

            logger.info("Logger info from deleteTrainee method");
        } else {
            throw new TraineeNotFoundException("Trainee with id " + trainee_id + "not found");
        }

        return temp;
    }

    public Trainee updateTrainee(Long trainee_id, String adress, Date date_birth, User user) throws TraineeNotFoundException {
        Trainee trainee = traineeRepository.findById(trainee_id)
                .orElseThrow(() -> new TraineeNotFoundException("Trainee not found with ID: " + trainee_id));

        trainee.setAddress(adress);
        trainee.setDate_birth(date_birth);
        trainee.setUser(user);

        logger.info("Logger info from deleteTrainee method");

        return traineeRepository.save(trainee);

    }

    public String generateUsername(String firstName, String lastName) {
        String baseUsername = firstName + "." + lastName;
        int serialNumber = 1;
        while (usernameExists(baseUsername + serialNumber)) {
            serialNumber++;
        }
        return baseUsername + serialNumber;
    }

    private boolean usernameExists(String username) {
        return traineeRepository.existsByUserName(username);
    }

    public String generatePassword() {

        int length = 10;
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuilder password = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            password.append(characters.charAt(random.nextInt(characters.length())));
        }

        return password.toString();
    }

}
