package com.alan.gym.repository;

import com.alan.gym.entity.Trainer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainerRepository extends JpaRepository<Trainer, Long>  {
    Boolean existsByUser_userName(String username);
}
