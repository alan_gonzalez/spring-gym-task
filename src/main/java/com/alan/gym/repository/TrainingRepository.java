package com.alan.gym.repository;

import com.alan.gym.entity.Training;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TrainingRepository extends JpaRepository<Training, Long> {
    
}
