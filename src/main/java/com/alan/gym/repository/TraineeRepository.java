package com.alan.gym.repository;

import com.alan.gym.entity.Trainee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TraineeRepository extends JpaRepository<Trainee, Long>{
    boolean existsByUserName(String username);
}
